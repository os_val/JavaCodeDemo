package util;

import java.io.InputStream;

/**
 * Created by zsq on 2017/9/26.
 */
public class StreamGobbler {

    private InputStream stream;

    private String msg;

    public StreamGobbler() {}

    public StreamGobbler(InputStream stream, String msg) {
        this.stream = stream;
        this.msg = msg;
    }

    public void start() {
    }
}
