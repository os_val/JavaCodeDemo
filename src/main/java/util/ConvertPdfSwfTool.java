package util;

import com.artofsolving.jodconverter.DocumentConverter;
import com.artofsolving.jodconverter.openoffice.connection.OpenOfficeConnection;
import com.artofsolving.jodconverter.openoffice.connection.SocketOpenOfficeConnection;
import com.artofsolving.jodconverter.openoffice.converter.StreamOpenOfficeDocumentConverter;

import java.io.File;
import java.io.IOException;

/**
 * 转换
 *
 * @author hill
 */
public class ConvertPdfSwfTool {

    // 将office（Txt/Word/Excel/PPT）类型转pdf,使用该方法前记得安装并开启openoffice，相关操作
    // 在cmd输入openoffice的相关命令，
    // 进入 cd C:\Program Files\OpenOffice 4\program回车
    // C:\Program Files\OpenOffice.org 3\program 本机：H:\Program Files (x86)\OpenOffice 4\program
    // 接着在复制soffice -headless -accept="socket,host=127.0.0.1,port=8100;urp;" -nofirststartwizard
    // -nofirststartwizard 到上面
    public static void convertPdf(File sourceFile, File pdfFile) {
        if (sourceFile.exists()) {
            if (!pdfFile.exists()) {
                //如果是window操作系统就启动openOffice服务
                if (isWindowsSystem()) {
                    String OpenOffice_HOME = Global.getConfig("WINDOWS_OPEN_OFFICE_HOME");//这里是OpenOffice的安装目录, 在我的项目中,为了便于拓展接口,没有直接写成这个样子,但是这样是绝对没问题的
                    // 如果从文件中读取的URL地址最后一个字符不是 '\'，则添加'\'
                    if (OpenOffice_HOME.charAt(OpenOffice_HOME.length() - 1) != '\\') {
                        OpenOffice_HOME += "\\";
                    }
                    // 启动OpenOffice的服务
                    String command = OpenOffice_HOME
                            + "program\\soffice.exe -headless -accept=\"socket,host=" + Global.getConfig("USE_OPEN_OFFICE_HOST") + ",port=8100;urp;\" -nofirststartwizard";
                    try {
                        Process pro = Runtime.getRuntime().exec(command);
                    } catch (IOException e2) {
                        System.out.println(e2);
                        //e2.printStackTrace();
                    }
                } else {
                    // 启动OpenOffice的服务
                    String command = "/opt/openoffice4/program/soffice -headless -accept=\"socket,host=127.0.0.1,port=8100;urp;\" -nofirststartwizard &";
                    try {
                        Process pro = Runtime.getRuntime().exec(command);
                    } catch (IOException e2) {
                        System.out.println(e2);
                        //	e2.printStackTrace();
                    }
                }

                boolean useOpenOfficeHost = "true".equalsIgnoreCase(Global.getConfig("USE_OPEN_OFFICE_HOST")) ? true : false;

                // connect to an OpenOffice.org instance running on port 8100
                OpenOfficeConnection connection;
                //如果存在远程服务器就调用远程服务器转换文件，否则调用本机转换服务
                if (useOpenOfficeHost) {
                    connection = new SocketOpenOfficeConnection(Global.getConfig("OPEN_OFFICE_HOST"), 8100);
                } else {
                    connection = new SocketOpenOfficeConnection(8100);
                }
                try {
                    //System.out.println("connection 开始："+ DateUtil.getDateTime());
                    connection.connect();
                    //System.out.println("converter  开始："+ DateUtil.getDateTime());
                    DocumentConverter converter = new StreamOpenOfficeDocumentConverter( connection);
                    //System.out.println("转换 开始时间："+ DateUtil.getDateTime());
                    converter.convert(sourceFile, pdfFile);
                    //System.out.println("转换 完成时间："+ DateUtil.getDateTime());
                    //System.out.println("第二步：转换为PDF格式 路径" + pdfFile.getPath());
                    //System.out.println("已成功转换为PDF");
                } catch (java.net.ConnectException e) {
                    e.printStackTrace();
                    //System.out.println("OpenOffice服务未启动");
                    // throw e;
                } catch (com.artofsolving.jodconverter.openoffice.connection.OpenOfficeException e) {
                    e.printStackTrace();
                    //System.out.println("读取文件失败");
                    throw e;
                } catch (Exception e) {
                    e.printStackTrace();
                    try {
                        throw e;
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                } finally {
                    if (connection != null) {
                        connection.disconnect();
                        connection = null;
                    }
                }
            } else {
                //System.out.println("已转换为PDF，无需再次转换");
            }
        } else {
            //System.out.println("要转换doc的文件不存在");
        }
    }

    public static void convertForType(String sourceFile) {

    }

    // pdf类型装换为swf类型的转换器
    // 使用前安装swfTool软件，并把其（如D:\docmanager\SWFTools;）配置到环境变量path里面
    // 进命令行输入pdf2swf判断环境变量是否设置成功，成功后需重启服务器(如果不行试试重新启动系统)。
    public static void convertPdfToSwf(File pdfFile, File swfFile) {
        if (!swfFile.exists()) {
            if (pdfFile.exists()) {
                try {
                    Process p = null;
                    // window
                    if (isWindowsSystem()) {
                        String exe = Global.getConfig("swftoolsWindowsPath") + "/pdf2swf.exe \"" + pdfFile.getPath()
                                + "\" -o \"" + swfFile.getPath() + "\" -T 9";
                        //System.out.println(exe);
                        String[] cmd = new String[]{"cmd", "/c", exe};
                        p = Runtime.getRuntime().exec(cmd);
                    }
                    // linux
                    else {
                        //pdf2swf -s languagedir=/usr/share/xpdf/xpdf-chinese-simplified -s flashversion=9 "test.pdf" -o "test.swf"

                        String command = Global.getConfig("swftoolsPath") + "/pdf2swf -s languagedir=" + Global.getConfig("simplifiedPath") + " -s flashversion=9 " + pdfFile.getPath() + " -o " + swfFile.getPath();
                        //System.out.println("command=="+command);
                        p = Runtime.getRuntime().exec(command);
                    }


                    // -------------------
                    StreamGobbler errorGobbler = new StreamGobbler(p
                            .getErrorStream(), "ERROR");
                    // 处理错误信息
                    errorGobbler.start();
                    StreamGobbler outGobbler = new StreamGobbler(p
                            .getInputStream(), "STDOUT");
                    // 处理输出流
                    outGobbler.start();
                    p.waitFor();
                    p.destroy();
                    swfFile.createNewFile();
                    //System.out.println("第三步：转换为SWF格式 路径：" + swfFile.getPath());
                    //System.out.println("第四步：转换为SWF格式 名称：" + swfFile.getName());
                    // 如有需要可对源文件进行删除
                    /*  if (pdfFile.exists()) { pdfFile.delete(); }*/

                } catch (Exception e) {
                    e.printStackTrace();
                    try {
                        throw e;
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
            } else {
                //System.out.println("PDF文件不存在，无法转换");
            }
        } else {
            //System.out.println("已经转为SWF文件，无需再次转换");
        }
    }

    // 双转化，将源文件转换为pdf，再转换为swf
    public static boolean convertPdfAndSwf(String sourceUrl) {
        String pdfString = sourceUrl.substring(0, sourceUrl.lastIndexOf("."))
                + ".pdf";
        String swfString = sourceUrl.substring(0, sourceUrl.lastIndexOf("."))
                + ".swf";
        convertPdfAndSwf(pdfString,swfString,sourceUrl);
        return false;
    }
    // 双转化，将源文件转换为pdf，再转换为swf
    public static boolean convertPdfAndSwf(String pdfString,String swfString,String sourceUrl) {
        File sourceFile = new File("" + sourceUrl);
        File pdfFile = new File(pdfString);
        File swfFile = new File(swfString);
        if (sourceFile.exists()) {

            ConvertPdfSwfTool.convertPdf(sourceFile, pdfFile);
            // convertForType(sourceUrl);
			ConvertPdfSwfTool.convertPdfToSwf(pdfFile, swfFile);
            return true;
        }
        return false;
    }

    // 单转换 直接转换为swf
    public static boolean convertSwf(String sourceUrl) {
        String swfString = sourceUrl.substring(0, sourceUrl.lastIndexOf("."))
                + ".swf";
        File sourceFile = new File("" + sourceUrl);
        File swfFile = new File(swfString);
        if (sourceFile.exists()) {
            ConvertPdfSwfTool.convertPdfToSwf(sourceFile, swfFile);
            return true;
        }
        return false;
    }

    // 返回swf文件
    public static String getSwfFile(String sourceUrl) {
        File sourceFile = new File("" + sourceUrl);
        if (sourceFile.exists()) {
            String swfString = sourceUrl.substring(0, sourceUrl
                    .lastIndexOf("."))
                    + ".swf";
            File swfFile = new File(swfString);
            if (swfFile.exists()) {
                return swfString;
            } else {
                convertPdfAndSwf(sourceUrl);
                return swfString;
            }
        }
        return null;
    }

    // 返回swf文件 url
    public static String getSwfURL(String sourceUrl) {
        String swfString = sourceUrl.substring(0, sourceUrl.lastIndexOf("."))
                + ".swf";
        return swfString;
    }

    // 返回后缀名
    public static String getSuffixName(String sourceUrl) {
        if (sourceUrl != null && !sourceUrl.equals("")) {
            return sourceUrl.substring(sourceUrl.lastIndexOf(".") + 1);
        }
        return "";
    }

    // 判断是否是图片类型
    public static boolean isPictureType(String sourceUrl) {
        String suffixName = getSuffixName(sourceUrl);
        if (suffixName.equals("jpg") || suffixName.equals("gif")
                || suffixName.equals("png")) {
            return true;
        }
        return false;
    }

    // 最终转换器
    public static void convertSwfTool(String sourceUrl) {
        if (isPictureType(sourceUrl) == false) {
            convertPdfAndSwf(sourceUrl);
        } else {
            convertSwf(sourceUrl);
        }
    }
    // 最终转换器
    public static void convertSwfTool(String pdfPath ,String swfPath,String sourceUrl) {
            convertPdfAndSwf(pdfPath,swfPath,sourceUrl);
    }

    // view：根目录下：FlexPaperViewer.swf
    // 以及Js目录下的flexpaper_flash.js等几个js方法以及swfobject文件
    //--------------------------------------------------------------------------
    // -----------------------------------

    /**
     * 判断是否是windows操作系统
     *
     * @return
     * @author iori
     */
    private static boolean isWindowsSystem() {
        String p = System.getProperty("os.name");
        return p.toLowerCase().indexOf("windows") >= 0 ? true : false;
    }

    public static void main(String[] a) {
        String filePath = "D:/经济补偿金如何计算(全).doc";
        ConvertPdfSwfTool.convertSwfTool(filePath);
        System.exit(0);
    }
}
