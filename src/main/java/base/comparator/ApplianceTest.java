package base.comparator;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by zsq on 2017/8/4.
 */
public class ApplianceTest {

    public static void main(String[] args) {

        List<ApplianceCategory> categoryList = new ArrayList<>();

        ApplianceCategory category = new ApplianceCategory();
        category.setApplianceCategory("微型计算机");
        category.setApplianceSubcategory("主机");
        categoryList.add(category);


        ApplianceCategory category2 = new ApplianceCategory();
        category2.setApplianceCategory("电视机");
        category2.setApplianceSubcategory("21寸");
        categoryList.add(category2);

        ApplianceCategory category3 = new ApplianceCategory();
        category3.setApplianceCategory("电视机");
        category3.setApplianceSubcategory("25寸");
        categoryList.add(category3);

        ApplianceCategory category4 = new ApplianceCategory();
        category4.setApplianceCategory("电视机");
        category4.setApplianceSubcategory("21寸");
        categoryList.add(category4);

        ApplianceCategory category5 = new ApplianceCategory();
        category5.setApplianceCategory("电视机");
        category5.setApplianceSubcategory("17寸");
        categoryList.add(category5);

        /**
         * 自定义对象去重
         */
        Set<ApplianceCategory> set = new HashSet<>();

        set.addAll(categoryList);

        System.out.println(set.size());

        for (ApplianceCategory categories : set) {
            System.out.println(categories.getApplianceCategory() +"----" + categories.getApplianceSubcategory());
        }
    }
}
