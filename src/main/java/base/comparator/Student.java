package base.comparator;

/**
 * Created by zsq on 2017/7/30.
 */
public class Student {

    private Integer id;

    private String name;

    private int age;

    private int grade;

    public Student() {}

    public Student(int id, String name, int age, int grade) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.grade = grade;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    @Override
    public int hashCode() {
       return name.hashCode();
    }

    /**
     * 对象集合去重 要重写 Object 的 hasCode() 和 equals() 方法
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        final Student student = (Student) obj;
        if (/* student.getId().intValue() == this.getId().intValue() &&*/
            student.getName().equals(this.getName()) && student.getAge() == student.getAge()) {
            return true;
        }
        return false;
    }
}
