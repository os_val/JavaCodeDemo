package base.comparator;

/**
 * Created by zsq on 2017/8/4.
 */
public class ApplianceCategory {

    private String applianceCategory;

    private String applianceSubcategory;

    public String getApplianceCategory() {
        return applianceCategory;
    }

    public void setApplianceCategory(String applianceCategory) {
        this.applianceCategory = applianceCategory;
    }

    public String getApplianceSubcategory() {
        return applianceSubcategory;
    }

    public void setApplianceSubcategory(String applianceSubcategory) {
        this.applianceSubcategory = applianceSubcategory;
    }

    @Override
    public int hashCode() {
        return (applianceCategory + applianceSubcategory).hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        ApplianceCategory appliance = (ApplianceCategory) obj;
        if (appliance.getApplianceCategory().equals(this.applianceCategory) &&
                appliance.getApplianceSubcategory().equals(this.applianceSubcategory)) {
            return true;
        }
        return false;
    }
}
