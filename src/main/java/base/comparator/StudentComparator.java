package base.comparator;

import java.util.Comparator;

/**
 * Created by zsq on 2017/7/30.
 */
public class StudentComparator implements Comparator<Student> {


    @Override
    public int compare(Student o1, Student o2) {
        int age1 = o1.getAge();
        int age2 = o2.getAge();
        //如果是集合对象进行排序, 则结果为从小到大的顺序
        if (age1 > age2) {  //大于
            return 1;
        }
        if (age1 < age2) {  //小于
            return -1;
        }
        return 0;  //等于
    }
}
