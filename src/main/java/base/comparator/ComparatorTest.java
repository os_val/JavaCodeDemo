package base.comparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by zsq on 2017/7/30.
 */
public class ComparatorTest {

    public static void main(String[] args) {

        List<Student> list = new ArrayList<>();

        Student s1 = new Student(1, "张三", 18, 5);
        Student s2 = new Student(1, "李四", 22, 5);
        Student s3 = new Student(1, "王五", 18, 6);
        Student s4 = new Student(1, "赵六", 28, 6);

        StudentComparator comparator = new StudentComparator();

        System.out.println(comparator.compare(s1, s2));  // -1 小于
        System.out.println(comparator.compare(s2, s3));  // 1 大于
        System.out.println(comparator.compare(s1, s3));  //等于


        list.add(s1); list.add(s2); list.add(s3);
        list.add(s4);

        //集合排序
        Collections.sort(list, comparator);

        for (Student student : list) {

            //结果为从小到大的顺序
            System.out.println("name="+student.getName()+", "+"age="+student.getAge());
        }
    }
}
