package base.innerouter;

import java.util.ArrayList;

/**
 * Created by zsq on 2017/12/11.
 * @author zsq
 * 内部类(静态内部类和非静态内部类)
 *
 */
public class OuterClass {

    private String sex;
    public static String name = "chenssy";

    /**
     *静态内部类
     */
    static class InnerClass1{
        /**
         * 在静态内部类中可以存在静态成员
         */
        public static String _name1 = "chenssy_static";

        public void display(){
            /*
             * 静态内部类只能访问外部类的静态成员变量和方法
             * 不能访问外部类的非静态成员变量和方法
             */
            System.out.println("OutClass name :" + name);
            //访问外部类的静态方法
            run();
        }
    }

    /**
     * 非静态内部类
     */
    class InnerClass2{
        /**
         * 非静态内部类中不能存在静态成员
         */
        public String _name2 = "chenssy_inner";

        /**
         *  非静态内部类中可以调用外部类的任何成员,不管是静态的还是非静态的
         */
        public void display(){
            System.out.println("OuterClass name：" + name);
            System.out.println("sex :"+ (sex = "女"));
        }
    }

    /**
     * @desc 外围类方法
     * @return void
     */
    public void display(){
        /* 外部类访问静态内部类：内部类. */
        System.out.println(InnerClass1._name1);

        /* 静态内部类 可以直接创建实例不需要依赖于外部类 */
        InnerClass1 innerClass1 = new InnerClass1();
        innerClass1.display();

        /* 非静态内部的创建需要依赖于外部类 */
        OuterClass.InnerClass2 inner2 = new OuterClass().new InnerClass2();
        /* 外部非静态内部类的成员需要使用非静态内部类的实例 */
        System.out.println(inner2._name2);
        inner2.display();
    }

    /**
     * 外部类的非静态方法
     */
    public static void run() {
        System.out.println("外部类的非静态方法");
    }

    public static void main(String[] args) {
        OuterClass outer = new OuterClass();
        outer.display();

        ArrayList<String> arrayString = new ArrayList<>();
        ArrayList<Integer> arrayInteger = new ArrayList<>();
        //true
        System.out.println(arrayString.getClass() == arrayInteger.getClass());
    }
}
