package base;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by zsq on 2017/12/28.
 */
public class MapTest {

    public static void main(String[] args) {
        Map<String,String> map=new HashMap<String, String>();
        map.put("1","a");
        map.put("2","b");
        map.put("3","c");
        map.put("4","d");
        map.put("5","e");
        map.put("6","f");
        map.put("7","g");
        map.put("8","h");
        map.put("9","j");

        //这里是生成键的视图  如果要读取还必须get一次
        long start = System.currentTimeMillis();
        Set<String> ls = map.keySet();
        for (String l : ls) {
            System.out.println("k="+l+" v="+map.get(l));
        }
        long end = System.currentTimeMillis();
        System.out.println("keySet遍历的时间:"+(end-start));


        //这里是生成键和映射关系的视图    不需要再get一次。所以效率明显快
        start = System.currentTimeMillis();
        Set<Map.Entry<String, String>> lv = map.entrySet();
        for(Map.Entry<String, String> l:lv) {
            System.out.println(l.getKey() + "-----" + l.getValue());
        }
        end = System.currentTimeMillis();
        System.out.println("entrySet遍历的时间:"+(end-start));
    }
}
