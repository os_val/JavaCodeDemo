package base.static_code;

/**
 * Created by zsq on 2017/12/11.
 * @author zsq
 */
public class TestStatic3 {

    public static int k = 0;
    public static TestStatic3 t1 = new TestStatic3("t1");
    public static TestStatic3 t2 = new TestStatic3("t2");
    public static int i = print("i");
    public static int n = 99;
    public int j = print("j");

    {
        print("构造块");
    }

    static {
        print("静态块");
    }

    public TestStatic3(String str) {
        System.out.println((++k)+":"+str+"    i="+i+"    n="+n);
        ++i;
        ++n;
    }

    private static int print(String str) {
        System.out.println((++k)+":"+str+"    i="+i+"    n="+n);
        ++n;
        return ++i;
    }

    public static void main(String[] args) {
        TestStatic3 t = new TestStatic3("init");
    }
}
