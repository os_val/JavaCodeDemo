package base;

/**
 * Created by zsq on 2018/8/13.
 */
public class DemoTest {
    public static void main(String[] args) {

        Person p = new Person("张三");

        change(p);

        System.out.println(p.name);

    }

    public static void change(Person p) {

        Person person = new Person("李四");

        p = person;
        //p.name="李四";

    }

}

class Person {

    String name;

    public Person(String name) {

        this.name = name;

    }

}
