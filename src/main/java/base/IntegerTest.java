package base;

import base.comparator.Student;
import util.DateUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by zsq on 2017/2/17.
 * Integer 对象的比较
 *
 *  Integer 类中有一个静态内部类IntegerCache, IntegerCachez中的属性low和high,最低为low -128  最高 high 127
 *
 *  Integer 进行数值比较时, 先检查是否在缓存数值(区间在 [-128, 127] )中, 如果在就直接比较数值大小, 如果不在就 比较的 对象地址.
 *
 *
 */
public class IntegerTest {

    public static void main(String[] args) throws ParseException {

        Integer a = 127;
        Integer b = 127;

        System.out.println(a == b);  // true


        Integer m = 128;
        Integer n = 128;

        System.out.println(m == n);

        //获取当天开始时间

        Calendar currentDate = new GregorianCalendar();

        currentDate.set(Calendar.HOUR_OF_DAY, 0);
        currentDate.set(Calendar.MINUTE, 0);
        currentDate.set(Calendar.SECOND, 0);
        System.out.println(currentDate.getTime());

        Date todayStartTime = null;
        Date todayEndTime = null;

        try {
            //一天的开始时间
            todayStartTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2015-11-01 00:00:01");
            //一天的结束时间
            Calendar rightNow = Calendar.getInstance();
            rightNow.setTime(todayStartTime);
            rightNow.add(Calendar.HOUR, 23);
            rightNow.add(Calendar.MINUTE, 59);
            rightNow.add(Calendar.SECOND, 58);
            System.out.println(todayStartTime);
            todayEndTime = rightNow.getTime();
            System.out.println(rightNow.getTime());
            System.out.println(todayStartTime);


            System.out.println("todayStart + 1");

            Calendar instance = Calendar.getInstance();
            instance.setTime(todayStartTime);
            instance.add(Calendar.DATE, 1);
            todayStartTime = instance.getTime();
            System.out.println(instance.getTime());

            System.out.println("todayEnd + 1");

            Calendar instance2 = Calendar.getInstance();
            instance2.setTime(todayEndTime);
            instance2.add(Calendar.DATE, 1);
            todayEndTime = instance2.getTime();
            System.out.println(instance2.getTime());

            System.out.println("--------相加之后的时间----------");
            System.out.println(todayStartTime+"-------"+todayEndTime);


            System.out.println("--------时间格式转化------------");

            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            Date time = format.parse("2016-12-12"+" 00:00:00");
            System.out.println(time);


            SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
            Date test = format2.parse("2017-08-22");
            Calendar instance3 = Calendar.getInstance();
            instance3.setTime(test);
            instance3.add(Calendar.DATE, 1);
            System.out.println(instance3.getTime());

            if (test.before(instance3.getTime())) {
                    System.out.println(true);
            }

            System.out.println("当前时间减去60天");

            Date nowTime = new Date();

            Calendar nowInstance = Calendar.getInstance();
            nowInstance.setTime(new Date());
            nowInstance.set(Calendar.HOUR_OF_DAY, 23);
            nowInstance.set(Calendar.MINUTE, 59);
            nowInstance.set(Calendar.SECOND, 59);


            Calendar instance4 = Calendar.getInstance();
            instance4.setTime(nowTime);
            instance4.add(Calendar.DAY_OF_MONTH, -60);

            System.out.println("当前时间:"+nowTime);
            System.out.println("当天最晚时间:"+nowInstance.getTime());
            System.out.println("当前时间减去60天后的时间:"+instance4.getTime());

            System.out.println("Math.ceil函数:"+Math.ceil(0.5));


            System.out.println(0.1234 > 0);

            String timeStr = DateUtils.getTimeStr(new Date(), "yyyy年MM月dd日HH时mm分");

            System.out.println(timeStr);
            System.out.println("年:"+ timeStr.substring(0, 4));
            System.out.println("月:"+ timeStr.substring(5, 7));
            System.out.println("日:"+ timeStr.substring(8, 10));
            System.out.println("时:"+ timeStr.substring(11, 13));
            System.out.println("分:"+ timeStr.substring(14, 16));


        } catch (ParseException e) {
            e.printStackTrace();
        }

        System.out.println("----------集合对象去重-------------");

        /**
         * 集合对象去重
         */

        List<Student> studentSet = new ArrayList<>();

        Student student = new Student();

        student.setId(1);
        student.setName("zhangsan");
        student.setAge(20);
        student.setGrade(5);
        studentSet.add(student);


        Student student1 = new Student();

        student1.setId(1);
        student1.setName("zhangsan");
        student1.setAge(20);
        student1.setGrade(6);
        studentSet.add(student1);

        Student student2 = new Student();

        student2.setId(2);
        student2.setName("lisi");
        student2.setAge(21);
        student1.setGrade(6);
        studentSet.add(student2);

        Student student3 = new Student();

        student3.setId(3);
        student3.setName("zhangsan");
        student3.setAge(20);
        student1.setGrade(6);
        studentSet.add(student3);

        System.out.println(studentSet.size());  // 4


        Set<Student> ts = new HashSet<>();
        ts.addAll(studentSet);
        System.out.println(ts.size()); // 2

        for (Student s : ts) {
            System.out.println(s.getName()+"-"+s.getAge());
        }

        Map<Long, Long> map = new ConcurrentHashMap<>();
        Long aa = 3L;
        Long bb = map.put(2L, aa);
        Long cc = map.put(2L, 4L);
        System.out.println(bb);  // null
        System.out.println(cc);  // 3


        List<String> list = new ArrayList<>();
        list.add("1");
        list.add("2");
        System.out.println("this is a list");
        for (String item : list) {
            if ("2".equals(item)) {
                list.remove(item);
            }
        }
        System.out.println(list);


    }



}
