package base;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by zsq on 2019/3/7.
 */
public class MapKeyTest {

    public static void main(String[] args) {
        Map<String, Object> result = new HashMap<>(16);
        String key = "abc12121";
        result.put(key, "123");
        result.put(key, "456");
        System.out.println(result);

        int a = hash(key);
        int b = hash(key);

        System.out.println(a);
        System.out.println(b);
        System.out.println(a == b);
    }

    static final int hash(Object key) {
        int h;
        return (key == null) ? 0 : (h = key.hashCode()) ^ (h >>> 16);
    }
}
