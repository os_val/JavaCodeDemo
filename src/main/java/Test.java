import util.DateUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Demo class
 *
 * @author zsq
 * @date 2017/11/1
 */
public class Test {


    public static void printIntValue(List<? extends Number> list) {
        for (Number number : list) {
            System.out.print(number.intValue()+" ");
        }
        System.out.println();
    }

    public static void main(String[] args) {
        Calendar nowInstance = Calendar.getInstance();
        nowInstance.setTime(new Date());
        nowInstance.set(Calendar.HOUR_OF_DAY, 23);
        nowInstance.set(Calendar.MINUTE, 59);
        nowInstance.set(Calendar.SECOND, 59);
        Date nowTime = nowInstance.getTime();

        Calendar instance = Calendar.getInstance();
        instance.setTime(nowTime);
        //当前时间减去60天
        instance.add(Calendar.DATE, -60);
        Date minTime = instance.getTime();
        System.out.println(minTime);

        Integer today = DateUtils.getDayInteger(DateUtils.parseDate("2017-11-03 23:59:59", "yyyy-MM-dd HH:mm:ss"));
        Calendar instance2 = Calendar.getInstance();
        instance2.setTime(DateUtils.parseDate("2017-11-03 23:59:59", "yyyy-MM-dd HH:mm:ss"));
        instance2.add(Calendar.DATE, -60);
        System.out.println(today);
        System.out.println(DateUtils.getDayInteger(instance2.getTime()));

        String str = new String(new char[1]);
        System.out.println(str);

        // 4 * 2^2
        int i = 4 << 2;
        // 9 / 2^3
        int j = 9 >> 3;
        System.out.println(i);
        System.out.println(j);

        System.out.println("泛型.....");

        List<Integer> integerList = new ArrayList<>();
        integerList.add(2);
        integerList.add(2);
        printIntValue(integerList);
        List<Float> floatList = new ArrayList<>();
        floatList.add((float) 3.3);
        floatList.add((float) 0.3);
        printIntValue(floatList);


        Calendar cal = Calendar.getInstance();

        try {
            cal.setTime(new SimpleDateFormat("yyyy-MM-dd").parse("2017-12-01"));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        int d = 0;
        if(cal.get(Calendar.DAY_OF_WEEK)==1){
            d = -6;
        }else{
            d = 2-cal.get(Calendar.DAY_OF_WEEK);
        }
        cal.add(Calendar.DAY_OF_WEEK, d);
        //所在周开始日期
        System.out.println(new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime()));
        cal.add(Calendar.DAY_OF_WEEK, d+6);
        //所在周结束日期
        System.out.println(new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime()));



        Calendar currentDate = new GregorianCalendar();
        currentDate.set(Calendar.HOUR_OF_DAY, 0);
        currentDate.set(Calendar.MINUTE, 0);
        currentDate.set(Calendar.SECOND, 0);
        Date endTime = currentDate.getTime();
        System.out.println(endTime);

        Date startTime = DateUtils.parseDate("2015-11-01 00:00:01", "yyyy-MM-dd HH:mm:ss");
        System.out.println(startTime);


        Date time1 = DateUtils.parseDate("2016-10-01 00:00:00", "yyyy-MM-dd HH:mm:ss");
        while (startTime.before(time1)) {
             System.out.println(DateUtils.getTimeStr(startTime, "yyyy-MM-dd HH:mm:ss"));

            startTime = DateUtils.datePlus(startTime, Calendar.DATE, 1);
        }

        String a = 20180307+"";
        String b = a.substring(0, 4)+"-"+a.substring(4,6)+"-"+a.substring(6,8)+" 00:00:00";
        Date c = DateUtils.parseDate(b, "yyyy-MM-dd HH:mm:ss");
        System.out.println(c);

        String aaa = "胡歌胡歌胡歌胡歌弩哥";
        System.out.println(aaa.substring(0, 5));

        List<String> strList = new ArrayList<>();
        strList.add("1");
        strList.add("2");
        for (String str1 : strList) {
            //删除 1 为正常
            if ("1".equals(str1)) {
                strList.remove(str1);
            }

            //Exception in thread "main" java.util.ConcurrentModificationException
            if ("2".equals(str1)) {
                strList.remove(str1);
            }
        }
    }
}
