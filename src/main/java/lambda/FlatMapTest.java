package lambda;

import java.util.Arrays;
import java.util.List;

/**
 * Created by zsq on 2017/3/22.
 */
public class FlatMapTest {

    public static void main(String[] args) {
        List<Integer> s1 = Arrays.asList(1, 2);
        List<String> s2 = Arrays.asList("A", "B");
        List<String> s3 = Arrays.asList("X", "Y");


        s1.stream().flatMap(v1 -> s2.stream().map(v2 -> new Tuple<>(v1, v2)))
                .forEach(System.out::println);

        System.out.println("------------------");

        s1.stream().flatMap(v1 -> s2.stream().map(v2 -> new Tuple<>(v1, v2)))
                .flatMap(v12 -> s3.stream().map(v3 -> new ThreeTuple<>(v12.a, v12.b, v3)))
                .forEach(System.out::println);
    }

}
