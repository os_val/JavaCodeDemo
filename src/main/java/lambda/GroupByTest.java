package lambda;

import lambda.bean.Employee;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by zsq on 2017/3/22.
 */
public class GroupByTest {

   public static void main(String[] args) {

       Employee emp1 = new Employee("Alice","London",200);
       Employee emp2 = new Employee("Bob","London",150);
       Employee emp3 = new Employee("Charles","New York",160);
       Employee emp4 = new Employee("Dorothy","Hong Kong",190);

       List<Employee> emps = new ArrayList<>();
       emps.add(emp1);
       emps.add(emp2);
       emps.add(emp3);
       emps.add(emp4);

       Map<String, List<Employee>> employeesByCity = emps.stream().collect(Collectors.groupingBy(Employee::getCity));

       for (Map.Entry<String, List<Employee>> entry : employeesByCity.entrySet()) {
            String cityGroup = entry.getKey();
            System.out.println("/*****************" + cityGroup + "********************/");
            List<Employee> employees = entry.getValue();
            for (Employee employee : employees) {
                System.out.println(employee.toString());
            }
           System.out.println();
       }
   }

}
