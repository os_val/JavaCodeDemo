package lambda;

import lambda.bean.Person;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by zsq on 2017/3/25.
 */
public class UnionAllTest {

    public static void main(String[] args) {

        Person person1 = new Person(1,"zhangsan",23);
        Person person2 = new Person(2,"zhangsan",26);
        Person person3 = new Person(3,"zhangsan",20);

        Person person4 = new Person(4,"zhangsan",27);
        Person person5 = new Person(5,"zhangsan",29);
        Person person6 = new Person(6,"zhangsan",30);

        List<Person> persons1 = new ArrayList<Person>();
        persons1.add(person1);
        persons1.add(person2);
        persons1.add(person3);

        List<Person> persons2 = new ArrayList<Person>();
        persons2.add(person4);
        persons2.add(person5);
        persons2.add(person6);

        Stream.concat(Stream.of(persons1.stream().collect(Collectors.toMap(Person::getId, Person::getSelf))),
                Stream.of(persons2.stream().collect(Collectors.toMap(Person::getId, Person::getSelf))))
                .forEach(System.out::println);
    }
}
