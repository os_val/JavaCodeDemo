package lambda;

/**
 * Created by zsq on 2017/3/22.
 */
public class ThreeTuple<A, B, C> extends Tuple<A, B>{

    public C c;

    public ThreeTuple(A a, B b, C c) {
        super(a, b);
        this.c = c;
    }

    @Override
    public String toString() {
        return "a:" + super.a + ",b:" + super.b + ",c:" + this.c;
    }
}
