package lambda;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

/**
 * Created by zsq on 2017/3/25.
 */
public class MyCollectors {

    public static void main(String[] args) {
        Stream<List<Integer>> inputStream = Stream.of(
                Arrays.asList(1),
                Arrays.asList(2, 3),
                Arrays.asList(4, 5, 6)
        );

        Stream<Integer> outputStream = inputStream.flatMap(v1 -> v1.stream());

    }
}
