package lambda;

import java.util.Arrays;
import java.util.List;

/**
 * Created by zsq on 2017/3/25.
 */
public class SortByTest {

    public static void main(String[] args) {
        List<Integer> s1 = Arrays.asList(1,2,6,7,0,4,9,3,8,5);

        s1.stream().sorted((o1, o2) -> o2.compareTo(o1))
                .forEach(System.out::println);

        System.out.println("------------------------");


        s1.stream().sorted((o1, o2) -> o1.compareTo(o2))
                .forEach(System.out::println);
    }
}
