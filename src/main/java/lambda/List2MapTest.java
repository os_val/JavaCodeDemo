package lambda;

import lambda.bean.Person;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by zsq on 2017/3/25.
 */
public class List2MapTest {

    public static void main(String[] args) {
        Person person1 = new Person(1,"zhangsan",23);
        Person person2 = new Person(2,"zhangsan",26);
        Person person3 = new Person(3,"zhangsan",20);
        List<Person> persons = new ArrayList<Person>();
        persons.add(person1);
        persons.add(person2);
        persons.add(person3);

        persons.stream().collect(Collectors.toMap(Person :: getId, Person::getSelf))
                .forEach((k, v) -> {
                    System.out.println(v);
                });
    }
}
