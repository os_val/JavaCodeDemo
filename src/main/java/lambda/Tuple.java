package lambda;

/**
 * Created by zsq on 2017/3/22.
 */
public class Tuple<A, B> {

    public A a;

    public B b;

    public Tuple(A a, B b) {
        this.a = a;
        this.b = b;
    }



    @Override
    public String toString() {
        return "a:" + this.a + ",b:" + this.b;
    }
}
