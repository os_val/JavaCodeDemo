package lambda;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * Created by zsq on 2017/3/25.
 */
public class InnerJoinTest {

    public static void main(String[] args) {
        List<Integer> s1 = Arrays.asList(1, 2);
        List<Integer> s2 = Arrays.asList(1, 3);

        s1.stream().flatMap(v1 -> s2.stream().filter(v2 -> Objects.equals(v1, v2))
                    .map(v2 -> new Tuple<>(v1, v2)))
                .forEach(System.out ::println);

    }
}
