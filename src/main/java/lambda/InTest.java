package lambda;

import lambda.bean.Person;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by zsq on 2017/3/25.
 * Simulate The in Query like SQL Query Expression
 */
public class InTest {

    public static void main(String[] args) {

        Person person1 = new Person(1, "zhangsan", 23);
        Person person2 = new Person(2, "lisi", 26);
        Person person3 = new Person(3, "wangwu", 20);
        Person person4 = new Person(4, "zhaoliu", 21);
        Person person5 = new Person(5, "yanqi", 28);

        List<Person> persons = new ArrayList<Person>();
        persons.add(person1);
        persons.add(person2);
        persons.add(person3);
        persons.add(person4);
        persons.add(person5);

        List<String> ids = Arrays.asList("1,3,4,5".split(","));

        persons.stream().filter(x -> ids.stream().mapToInt(i -> Integer.valueOf(i))
                .anyMatch(id -> id == x.getId())).
                forEach(System.out::println);
    }
}
