package lambda;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * Created by zsq on 2017/3/25.
 */
public class LeftJoinTest {

    public static void main(String[] args) {
        List<Integer> s1 = Arrays.asList(1,2,3,5,7,9);
        List<Integer> s2 = Arrays.asList(1,3,5,9);

        s1.stream()
                .map(v1 ->
                        {
                            if(!s2.contains(v1)) {
                                return new Tuple(v1,null);
                            }
                            /*for(int v2 : s2) {
                                if(Objects.equals(v2,v1)) {
                                    return new Tuple(v1,v2);
                                }
                            }
                            return null;*/
                            return s2.stream().filter(v2-> Objects.equals(v2,v1))
                                    .map(v2-> new Tuple(v1,v2)).findAny().get();
                        }
                )
                .forEach(System.out::println);
    }
}
