package lambda.bean;

/**
 * Created by zsq on 2017/3/25.
 */
public class Person {

    private int id;
    private String name;
    private int age;

    public Person(int id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public Person getSelf() {
        return this;
    }

    @Override
    public String toString() {
        return "Person:[id]->" + id + ",[namge]->" + name + ",[age]->" + age;
    }

}
