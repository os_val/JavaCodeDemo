package lambda.bean;

/**
 * Created by zsq on 2017/3/22.
 */
public class Employee {

    /**员工姓名*/
    private String name;
    /**城市*/
    private String city;
    /**销售额*/
    private int sale;

    public Employee(String name, String city, int sale) {
        this.name = name;
        this.city = city;
        this.sale = sale;
    }

    public String getName() {
        return name;
    }

    public String getCity() {
        return city;
    }

    public int getSale() {
        return sale;
    }

    @Override
    public String toString() {
        return "Employee:[name]:" + name + ",[city]:" + city + ",[sale]:" + sale;
    }
}
