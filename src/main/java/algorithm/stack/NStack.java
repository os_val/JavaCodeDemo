package algorithm.stack;

/**
 * Created by zsq on 2018/1/15.
 */
public class NStack {

    private int[] stack = new int[100];
    private int top = 0;
    private int max = 0;

    public void push(int el) {
        stack[++top] = el - max;

        if (el > max) {
            max = el;
        }
    }

    public int pop() {
        int t = stack[top] + max;

        if (stack[top] > 0) {
            t = max;
            max -= stack[top--];
        } else {
            top -- ;
        }
        return t;
    }

    public int getMax() {
        return max;
    }

    public static void main(String[] args) {
        NStack stack = new NStack();
        stack.push(1);
        stack.push(4);
        stack.pop();
        stack.push(7);
        stack.push(2);
        stack.push(9);
        stack.pop();
        System.out.println("max的值为:"+stack.getMax());
    }
}
