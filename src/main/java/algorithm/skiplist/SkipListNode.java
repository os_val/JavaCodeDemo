package algorithm.skiplist;

/**
 * Created by zsq on 2017/6/27.
 */
public class SkipListNode<T> {

    public T key;

    public int level;

    public SkipListNode<T>[] next;

    public SkipListNode(){};

    public SkipListNode(T key, int level) {
        this.key = key;
        this.level = level;
    }
}
