package algorithm.skiplist;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.ConcurrentSkipListSet;

/**
 * Created by zsq on 2017/6/27.
 *
 * 跳表（SkipList）这种数据结构算是以前比较少听说过，它所实现的功能与红黑树，AVL树都差不太多，说白了就是一种基于排序的索引结构，
 * 它的统计效率与红黑树差不多，但是它的原理，实现难度以及编程难度要比红黑树简单。
 * 另外它还有一个平衡的树形索引机构没有的好处，这也是引导自己了解跳表这种数据结构的原因，就是在并发环境下其表现很好.
 * 这里可以想象，在没有了解SkipList这种数据结构之前，如果要在并发环境下构造基于排序的索引结构，那么也就红黑树是一种比较好的选择了，
 * 但是它的平衡操作要求对整个树形结构的锁定，因此在并发环境下性能和伸缩性并不好.
 * 在Java中，skiplist提供了两种：
 * ConcurrentSkipListMap 和 ConcurrentSkipListSet
 * 两者都是按自然排序输出。
 *
 *
 */
public class SkipListDemo {

    public static void skipListMapShow() {

        Map<Integer, String> map = new ConcurrentSkipListMap<>();

        map.put(1, "1");
        map.put(23, "23");
        map.put(3, "3");
        map.put(2, "2");
        //输出的是有序的key
        map.keySet().forEach(System.out::println);
    }

    public static void skipListSetShow() {
       Set<Integer> set = new ConcurrentSkipListSet<>();

        set.add(1);
        set.add(21);
        set.add(6);
        set.add(2);

        set.forEach(System.out::println);  //输出有序的set值

        Set<String> strSet = new ConcurrentSkipListSet<>();

        strSet.add("abc");
        strSet.add("fgi");
        strSet.add("def");
        strSet.add("Abc");
        strSet.add("FGI");
        strSet.add("Fgi");

        strSet.forEach(System.out::println);  // Abc FGI Fgi abc def fgi
    }

    public static void main(String[] args) {
        //skipListMapShow();
        skipListSetShow();
    }
}
