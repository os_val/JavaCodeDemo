package java8.stream;

/**
 * Created by zsq on 2018/1/24.
 */
public class Employee {

    private String name;

    private String city;

    private Double sales;

    public Employee() {

    }

    public Employee(String name, String city, Double sales) {
        this.name = name;
        this.city = city;
        this.sales = sales;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Double getSales() {
        return sales;
    }

    public void setSales(Double sales) {
        this.sales = sales;
    }
}
