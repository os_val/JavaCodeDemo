package java8.stream;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by zsq on 2018/1/24.
 */
public class EmployeeStream {

    public static void main(String[] args) {
        List<Employee> employeeList = new ArrayList<>();

        Employee employee1 = new Employee("Alice", "London", 200d);
        Employee employee2 = new Employee("Bob", "London", 150d);
        Employee employee3 = new Employee("Charles", "New York", 160d);
        Employee employee4 = new Employee("Dorothy", "Hong Kong", 190d);
        Employee employee5 = new Employee("Lily", "London", 350d);
        employeeList.add(employee1);
        employeeList.add(employee2);
        employeeList.add(employee3);
        employeeList.add(employee4);
        employeeList.add(employee5);

        Map<String, List<Employee>> result = new HashMap<>();
        for (Employee e : employeeList) {
            String city = e.getCity();
            List<Employee> empsInCity = result.get(city);
            if (empsInCity == null) {
                empsInCity = new ArrayList<>();
                result.put(city, empsInCity);
            }
            empsInCity.add(e);
        }

        //stream 进行按城市分组 两种写法相同
        Map<String, List<Employee>> streamMap = employeeList.stream().collect(
                Collectors.groupingBy(employee -> employee.getCity()));
        Map<String, List<Employee>> streamMap2 = employeeList.stream().collect(
                Collectors.groupingBy(Employee::getCity));
        System.out.println(streamMap);
        System.out.println(streamMap2);

        //stream 计算每个城市雇员的数量
        Map<String, Long> employeeNum = employeeList.stream().collect(
                Collectors.groupingBy(Employee::getCity, Collectors.counting()));
        System.out.println(employeeNum);

        //stream 计算每个城市的薪资
        Map<String, Double> citySales = employeeList.stream().collect(
                Collectors.groupingBy(Employee::getCity, Collectors.averagingDouble(Employee::getSales)));
        System.out.println(citySales);


        //stream 进行分区操作
        Map<Boolean, List<Employee>> partitioned = employeeList.stream().collect(
                Collectors.partitioningBy(em -> em.getSales() > 160));
        System.out.println(partitioned);



        Map<Boolean, Map<String, Long>> partitionedMap = employeeList.stream().collect(
                Collectors.partitioningBy(em -> em.getSales() > 160,
                        Collectors.groupingBy(Employee::getCity, Collectors.counting())));
        System.out.println(partitionedMap);


        //对所有人进行求和操作
        Double totalSales = employeeList.stream().
                mapToDouble(Employee::getSales).sum();
        System.out.println(totalSales);


        //stream 进行filter操作
        List<Employee> filterList = employeeList.stream().filter(
                employee -> employee.getCity().contains("London"))
                .collect(Collectors.toList());
        System.out.println(filterList);

        /**
         * List 转 Map
         * toMap 如果集合对象有重复的key，会报错Duplicate key ....
         * 可以用 (k1,k2)->k1 来设置，如果有重复的key,则保留key1,舍弃key2
         */
        Map<String, Employee> employeeMap = employeeList.stream().
                collect(Collectors.toMap(Employee::getName, em -> em, (k1, k2) -> k1));
        System.out.println(employeeMap);
    }
}
