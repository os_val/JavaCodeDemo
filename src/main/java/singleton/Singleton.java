package singleton;


import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * Created by zsq on 2017/12/7.
 * @author zsq
 *
 * 懒汉模式实现单例模式
 *
 */
public class Singleton {

    /**
     * 单例对象
     * volatile修饰符阻止了变量访问前后的指令重排,保证了指令执行顺序
     */
    private volatile static Singleton instance = null;

    /**
     * 私有构造方法
     */
    private Singleton() {}

    /**
     * 静态方法工厂
     */
    public static Singleton getInstance() {
        //双重检测机制
        if (instance == null) {

            //同步锁
            synchronized (Singleton.class) {

                //双重检测机制
                if (instance == null) {

                    instance = new Singleton();

                }
            }


        }
        return instance;
    }

    public static void main(String[] args) {
       //获得构造器
       Constructor[] constructors = Singleton.class.getDeclaredConstructors();
       Constructor constructor = constructors[0];
       //设置可访问
       constructor.setAccessible(true);
        try {
            Singleton singleton1 = (Singleton) constructor.newInstance(new Object[]{});
            Singleton singleton2 = (Singleton) constructor.newInstance(new Object[]{});
            //false
            System.out.println(singleton1.equals(singleton2));
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }
}




