package singleton;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * Created by zsq on 2017/12/7.
 * @author zsq
 * 静态内部类实现单例模式
 */
public class InnerSingleton {

    /**
     * 静态内部类
     */
    private static class LazyHolder {

        private static final InnerSingleton INSTANCE = new InnerSingleton();
    }

    private InnerSingleton(){}

    public static InnerSingleton getInstance() {
        return LazyHolder.INSTANCE;
    }

    public static void main(String[] args) {
        //获得构造器
        Constructor[] constructors = InnerSingleton.class.getDeclaredConstructors();
        Constructor constructor = constructors[1];
        //设置可访问
        constructor.setAccessible(true);
        try {
            InnerSingleton singleton1 = (InnerSingleton) constructor.newInstance(new Object[]{});
            InnerSingleton singleton2 = (InnerSingleton) constructor.newInstance(new Object[]{});
            //false
            System.out.println(singleton1.equals(singleton2));
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }
}
