package singleton;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * Created by zsq on 2017/12/7.
 * @author zsq
 * 用枚举实现单例模式
 */
public enum SingletonEnum {
    INSTANCE;


    public static void main(String[] args) {
        //获得构造器
        Constructor[] constructors = SingletonEnum.class.getDeclaredConstructors();
        Constructor constructor = constructors[0];
        //设置可访问
        constructor.setAccessible(true);
        try {
            /**
             * JVM 会阻止反射获取枚举类的私有构造方法
             */
            //java.lang.IllegalArgumentException: Cannot reflectively create enum objects
            SingletonEnum singleton1 = (SingletonEnum) constructor.newInstance(new Object[]{});
            SingletonEnum singleton2 = (SingletonEnum) constructor.newInstance(new Object[]{});
            System.out.println(singleton1.equals(singleton2));
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }
}
