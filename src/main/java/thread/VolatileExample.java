package thread;

/**
 * Created by zsq on 2018/1/26.
 * @author zsq
 */
public class VolatileExample {

    int a = 0;
    volatile boolean flag = false;

    public void writer() {
        a = 1;
        flag = true;
    }

    public void read() {
        if (flag) {
            int i = a;
        }
    }
}










