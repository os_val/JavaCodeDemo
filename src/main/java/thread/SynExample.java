package thread;

/**
 * Created by zsq on 2018/1/24.
 * @author  zsq
 */
public class SynExample {

    int a = 0;
    boolean flag = false;


    public synchronized void writer() {  //获取锁
        a = 1;
        flag = true;
    }   //释放锁

    public synchronized void read() {  //获取锁
        if (flag) {
            int i = a;
        }
    }   //释放锁
}
