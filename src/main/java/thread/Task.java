package thread;

/**
 * Created by zsq on 2017/9/9.
 * 多个任务
 */
public class Task implements Runnable{


    private int num;

    public Task(int n) {
        this.num = n;
    }


    @Override
    public void run() {
        System.out.println("Task " + num + " is running.");
    }
}
