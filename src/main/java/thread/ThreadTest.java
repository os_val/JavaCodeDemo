package thread;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by zsq on 2018/1/2.
 * @author zsq
 */
public class ThreadTest {

    //原子类
    public static AtomicInteger count = new AtomicInteger(0);

    public static void main(String[] args) {
        for (int i = 0; i < 100; i++) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    //每个线程当中让线程自增100
                    for (int j = 0; j < 100; j++) {
                        //使用原子类操作来替换同步锁,保证数据一致
                        count.incrementAndGet();
                    }
                }
            }).start();
        }
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // count值是10000
        System.out.println("count="+count);
    }
}
