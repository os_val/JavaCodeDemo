package thread;

/**
 * Created by zsq on 2017/9/9.
 */
public class TaskMain {

    public static void main(String[] args) {
        ThreadPool threadPool = new ThreadPool(7);

        for (int i = 0; i < 5; i++) {
            Task task = new Task(i);
            threadPool.execute(task);
        }
    }
}
