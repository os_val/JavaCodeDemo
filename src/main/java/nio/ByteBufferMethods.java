package nio;

import java.nio.ByteBuffer;

/**
 * Created by zsq on 2018/5/22.
 */
public class ByteBufferMethods {

    public static void main(String[] args) {

        //分配缓冲区（Allocating a Buffer）
        ByteBuffer buffer = ByteBuffer.allocate(33);

        //ByteBuff:      java.nio.HeapByteBuffer[pos=0 lim=33 cap=33]
        System.out.println("ByteBuff:      " + buffer);

        System.out.println("-------------Test reset-------------");

        //clear()方法，position将被设回0，limit被设置成 capacity的值
        buffer.clear();

        // 设置这个缓冲区的位置
        buffer.position(5);

        //将此缓冲区的标记设置在其位置。(没有buffer.mark();这句话会报错, JDK1.8不会)
        buffer.mark();

        buffer.position(10);

        //before reset:      java.nio.HeapByteBuffer[pos=10 lim=33 cap=33]
        System.out.println("before reset:      " + buffer);

        //将此缓冲区的位置重置为先前标记的位置。（buffer.position(5)）
        buffer.reset();

        //after reset:       java.nio.HeapByteBuffer[pos=5 lim=33 cap=33]
        System.out.println("after reset:       " + buffer);

        System.out.println("-------------Test rewind-------------");

        buffer.clear();

        buffer.position(10);

        //返回此缓冲区的限制。
        buffer.limit(15);

        //before rewind:       java.nio.HeapByteBuffer[pos=10 lim=15 cap=33]
        System.out.println("before rewind:       " + buffer);

        //把position设为0，mark设为-1，不改变limit的值
        buffer.rewind();

        //before rewind:       java.nio.HeapByteBuffer[pos=0 lim=15 cap=33]
        System.out.println("before rewind:       " + buffer);

        System.out.println("-------------Test compact-------------");

        buffer.clear();

        buffer.put("abcd".getBytes());

        //before compact:       java.nio.HeapByteBuffer[pos=4 lim=33 cap=33]
        System.out.println("before compact:       " + buffer);

        //abcd                             
        System.out.println(new String(buffer.array()));

        //limit = position;position = 0;mark = -1; 翻转，也就是让flip之后的position到limit这块区域变成之前的0到position这块，
        //翻转就是将一个处于存数据状态的缓冲区变为一个处于准备取数据的状态
        buffer.flip();

        //after flip:       java.nio.HeapByteBuffer[pos=0 lim=4 cap=33]
        System.out.println("after flip:       " + buffer);

        //get()方法：相对读，从position位置读取一个byte，并将position+1，为下次读写作准备

        //a
        System.out.println((char) buffer.get());

        //b
        System.out.println((char) buffer.get());

        //c
        System.out.println((char) buffer.get());

        //after three gets:       java.nio.HeapByteBuffer[pos=3 lim=4 cap=33]
        System.out.println("after three gets:       " + buffer);

        //abcd                             
        System.out.println("\t" + new String(buffer.array()));

        //把从position到limit中的内容移到0到limit-position的区域内，position和limit的取值也分别变成limit-position、capacity。

        //如果先将positon设置到limit，再compact，那么相当于clear()
        buffer.compact();

        //after compact:       java.nio.HeapByteBuffer[pos=1 lim=33 cap=33]
        System.out.println("after compact:       " + buffer);

        //dbcd                             
        System.out.println("\t" + new String(buffer.array()));


        System.out.println("-------------Test get-------------");

        buffer = ByteBuffer.allocate(32);

        buffer.put((byte) 'a').put((byte) 'b').put((byte) 'c').put((byte) 'd')

                .put((byte) 'e').put((byte) 'f');

        //before flip():       java.nio.HeapByteBuffer[pos=6 lim=32 cap=32]
        System.out.println("before flip():       " + buffer);

        // 转换为读取模式
        buffer.flip();

        //before get():       java.nio.HeapByteBuffer[pos=0 lim=6 cap=32]
        System.out.println("before get():       " + buffer);

        //a
        System.out.println((char) buffer.get());

        //after get():       java.nio.HeapByteBuffer[pos=1 lim=6 cap=32]
        System.out.println("after get():       " + buffer);

        // get(index)不影响position的值
        // c
        System.out.println((char) buffer.get(2));

        //after get(index):       java.nio.HeapByteBuffer[pos=1 lim=6 cap=32]
        System.out.println("after get(index):       " + buffer);

        byte[] dst = new byte[10];

        buffer.get(dst, 0, 2);

        //after get(dst, 0, 2):       java.nio.HeapByteBuffer[pos=3 lim=6 cap=32]
        System.out.println("after get(dst, 0, 2):       " + buffer);

        //dst:bc        
        System.out.println("\t dst:" + new String(dst));

        //buffer now is:       java.nio.HeapByteBuffer[pos=3 lim=6 cap=32]
        System.out.println("buffer now is:       " + buffer);

        //abcdef                          
        System.out.println("\t" + new String(buffer.array()));


        System.out.println("-------------Test put-------------");

        ByteBuffer bb = ByteBuffer.allocate(32);

        //before put(byte):       java.nio.HeapByteBuffer[pos=0 lim=32 cap=32]
        System.out.println("before put(byte):       " + bb);

        //after put(byte):       java.nio.HeapByteBuffer[pos=1 lim=32 cap=32]
        System.out.println("after put(byte):       " + bb.put((byte) 'z'));

        //java.nio.HeapByteBuffer[pos=1 lim=32 cap=32]
        System.out.println("\t" + bb.put(2, (byte) 'c'));

        // put(2,(byte) 'c')不改变position的位置
        //after put(2,(byte) 'c'):       java.nio.HeapByteBuffer[pos=1 lim=32 cap=32]
        System.out.println("after put(2,(byte) 'c'):       " + bb);

        //z c                             
        System.out.println("\t" + new String(bb.array()));

        // 这里的buffer是 abcdef[pos=3 lim=6 cap=32]
        bb.put(buffer);

        //after put(buffer):       java.nio.HeapByteBuffer[pos=4 lim=32 cap=32]
        System.out.println("after put(buffer):       " + bb);

        //zdef                            
        System.out.println("\t" + new String(bb.array()));
    }
}
